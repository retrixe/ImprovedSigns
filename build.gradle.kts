import com.github.jengelman.gradle.plugins.shadow.tasks.ShadowJar

plugins {
    java
    kotlin("jvm") version "2.0.10"
    id("com.gradleup.shadow") version "8.3.0"
    id("net.kyori.blossom") version "2.1.0"
    id("org.jetbrains.gradle.plugin.idea-ext") version "1.1.8" // IntelliJ + Blossom integration
    id("org.ajoberstar.grgit.service") version "5.2.0"
}

group = "com.retrixe"
version = "1.0.0-alpha.0${getVersionMetadata()}"

description = "A plugin to improve Minecraft signs and item frames."

repositories {
    mavenCentral()
    maven { url = uri("https://repo.papermc.io/repository/maven-public/") }
    maven { url = uri("https://repo.codemc.org/repository/maven-public/") }
}

dependencies {
    compileOnly("io.papermc.paper:paper-api:1.20.4-R0.1-SNAPSHOT")
    implementation("io.github.bananapuncher714:nbteditor:7.19.3")
}

java {
    toolchain.languageVersion.set(JavaLanguageVersion.of(17))
}

sourceSets {
    main {
        blossom {
            resources {
                property("version", project.version.toString())
                property("description", project.description)
            }
        }
    }
}

tasks.withType<ShadowJar> {
    minimize()
    manifest {
        attributes(mapOf("paperweight-mappings-namespace" to "mojang"))
    }
    archiveClassifier.set("")

    relocate("kotlin", "com.retrixe.improvedsigns.kotlin")
    relocate("io.github.bananapuncher714.nbteditor", "com.retrixe.improvedsigns")
}

fun getVersionMetadata(): String {
    if (project.hasProperty("skipVersionMetadata")) return ""

    val grgit = try { grgitService.service.orNull?.grgit } catch (e: Exception) { null }
    if (grgit != null) {
        val head = grgit.head() ?: return "+unknown" // No head, fresh git repo
        var id = head.abbreviatedId
        val tag = grgit.tag.list().find { head.id == it.commit.id }

        // If we're on a tag and the tree is clean, don't put any metadata
        if (tag != null && grgit.status().isClean) {
            return ""
        }
        // Flag the build if the tree isn't clean
        if (!grgit.status().isClean) {
            id += "-dirty"
        }

        return "+git.$id"
    }

    return "+unknown"
}
