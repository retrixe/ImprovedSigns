# ImprovedSigns

A plugin to improve Minecraft signs and item frames.

This is a port of the [Improved Signs Fabric mod](https://www.curseforge.com/minecraft/mc-mods/improved-signs) to Bukkit, and is fully compatible with the mod (so you can move your Fabric worlds to Paper without a hitch with signs).

Join the Discord at: [https://discord.gg/MFSJa9TpPS](https://discord.gg/MFSJa9TpPS)

## Features

- Sign Editor: Edit placed signs by shift(sneak) + right-click with an empty hand
- Stop Sign editor from opening when placing new signs (disabled by default)
- Sign Passthrough: Access chests, shulker boxes, barrels, etc behind signs by right-clicking the sign
- Item Frame Passthrough: Interact with Item Frames normally by shift (sneak) + right-click
- Signs Retain: Breaking a sign will retain that sign's text, color and glowing. Put the sign in a crafting window to reset it
- Sign Copy: Right-clicking a sign with a sign (or stack of signs) in your hand will copy the text to the held signs
- Invisible Item Frames: Use Amethyst Shards to turn item frames invisible.

## Installation

Download the latest JAR of the plugin [from here](https://gitlab.com/retrixe/ImprovedSigns/-/releases), and place it in your `plugins` folder.

You can edit the `config.yml` file in `plugins/ImprovedSigns` to disable or enable any features you like.

## Acknowledgment

- [The Improved Signs Fabric mod for prior art](https://www.curseforge.com/minecraft/mc-mods/improved-signs)

## License

```
ImprovedSigns - A plugin to improve Minecraft signs and item frames.
    Copyright (C) 2022 retrixe

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
```
