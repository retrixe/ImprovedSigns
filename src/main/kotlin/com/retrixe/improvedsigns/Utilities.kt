package com.retrixe.improvedsigns

import com.retrixe.improvedsigns.signs.SignUtilsPost20
import com.retrixe.improvedsigns.signs.SignUtilsPre20
import net.kyori.adventure.text.Component
import net.kyori.adventure.text.serializer.gson.GsonComponentSerializer
import net.kyori.adventure.text.serializer.legacy.LegacyComponentSerializer
import net.kyori.adventure.text.serializer.plain.PlainTextComponentSerializer
import org.bukkit.Bukkit
import org.bukkit.Material
import org.bukkit.NamespacedKey
import org.bukkit.block.Block
import org.bukkit.block.Container
import org.bukkit.block.data.type.HangingSign
import org.bukkit.block.data.type.Sign
import org.bukkit.entity.Player
import org.bukkit.inventory.ItemStack
import org.bukkit.inventory.ShapelessRecipe

val majorVersion = Bukkit.getBukkitVersion().substringBefore("-").split(".")[1].toInt()

val SignUtils = if (majorVersion >= 20) SignUtilsPost20 else SignUtilsPre20

val signs = Material.entries.filter {
    if (it.name.contains("SIGN")) {
        val blockData = it.createBlockData()
        blockData is Sign || (majorVersion >= 20 && blockData is HangingSign)
    } else false
}

fun createSignRecipes() {
    for (sign in signs) {
        val key = NamespacedKey(ImprovedSigns.plugin, sign.name.lowercase())
        val recipe = ShapelessRecipe(key, ItemStack(sign))
        recipe.addIngredient(sign)
        if (!ImprovedSigns.plugin.server.addRecipe(recipe)) {
            ImprovedSigns.plugin.logger.warning("Failed to add recipe $key!")
        }
    }
}

fun openBlock(player: Player, block: Block): Boolean {
    val blockState = block.state
    val blockType = block.type
    if (blockState is Container) {
        player.openInventory(blockState.inventory)
    } else if (blockType == Material.ENDER_CHEST) {
        player.openInventory(player.enderChest)
    } else if (blockType == Material.ENCHANTING_TABLE) {
        player.openEnchanting(block.location, false)
    } else if (blockType == Material.ANVIL ||
        blockType == Material.CHIPPED_ANVIL ||
        blockType == Material.DAMAGED_ANVIL) {
        player.openAnvil(block.location, false)
    } else if (blockType == Material.GRINDSTONE) {
        player.openGrindstone(block.location, false)
    } else if (blockType == Material.CRAFTING_TABLE) {
        player.openWorkbench(block.location, false)
    } else if (blockType == Material.STONECUTTER) {
        player.openStonecutter(block.location, false)
    } else if (blockType == Material.LOOM) {
        player.openLoom(block.location, false)
    } else if (blockType == Material.CARTOGRAPHY_TABLE) {
        player.openCartographyTable(block.location, false)
    } else if (blockType == Material.SMITHING_TABLE) {
        player.openSmithingTable(block.location, false)
    } else return false
    return true
}

fun isComponentBlank(component: Component) =
    PlainTextComponentSerializer.plainText().serialize(component).isBlank()

fun deserialiseText(text: String) = try {
    GsonComponentSerializer.gson().deserialize(text)
} catch (e: Exception) {
    LegacyComponentSerializer.legacySection().deserialize(text)
}

fun isSignNotEmpty(sign: org.bukkit.block.Sign) =
    SignUtils.isSignNotEmpty(sign)
