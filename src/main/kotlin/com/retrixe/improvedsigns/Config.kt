package com.retrixe.improvedsigns

import net.kyori.adventure.text.minimessage.MiniMessage
import org.bukkit.Material
import java.lang.IllegalArgumentException

object Config {
    fun reloadConfig() {
        val plugin = ImprovedSigns.plugin
        plugin.saveDefaultConfig()
        plugin.reloadConfig()

        // Load config options
        val config = plugin.config

        // Sign editor options
        signEditorEnabled = config.getBoolean("sign-editor.enabled", true)
        signEditorBlockOnPlacingSign = config.getBoolean("sign-editor.block-on-placing-sign", false)

        // Passthrough options
        passthroughSigns = config.getBoolean("passthrough.signs", true)
        passthroughItemFrames = config.getBoolean("passthrough.item-frames", true)

        // Signs retain
        signsRetainEnabled = config.getBoolean("signs-retain.enabled", true)
        signsRetainKeepDyeWhenNoText = config.getBoolean("signs-retain.keep-dye-when-no-text", true)

        // Sign copy
        signCopyEnabled = config.getBoolean("sign-copy.enabled", true)
        signCopyDyeGlowWax = config.getBoolean("sign-copy.copy-dye-glow-wax", false)

        // Invisible item frames
        invisibleItemFramesEnabled = config.getBoolean("invisible-item-frames.enabled", true)
        val invisibleItemFramesItemValue =
            config.getString("invisible-item-frames.item") ?: "AMETHYST_SHARD"
        try {
            invisibleItemFramesItem = Material.valueOf(invisibleItemFramesItemValue)
        } catch (e: IllegalArgumentException) {
            invisibleItemFramesItem = Material.AMETHYST_SHARD
            plugin.logger.severe("Config invisible-item-frames.item has invalid value," +
                    " using AMETHYST_SHARD instead: " +
                    invisibleItemFramesItemValue)
        }

        // Language definitions
        langPrefix = MiniMessage.miniMessage()
            .deserialize(config.getString("lang.prefix") ?: "")
        langSignCopy = MiniMessage.miniMessage()
            .deserialize(config.getString("lang.sign-copy") ?: "")
        langSignContentsRetained = MiniMessage.miniMessage()
            .deserialize(config.getString("lang.sign-contents-retained") ?: "")
        langSignContentsRestored = MiniMessage.miniMessage()
            .deserialize(config.getString("lang.sign-contents-restored") ?: "")
        langItemFrameVisibilityChange = MiniMessage.miniMessage()
            .deserialize(config.getString("lang.item-frame-visibility-change") ?: "")
    }

    var signEditorEnabled = true
    var signEditorBlockOnPlacingSign = false

    var passthroughSigns = true
    var passthroughItemFrames = true

    var signsRetainEnabled = true
    var signsRetainKeepDyeWhenNoText = true

    var signCopyEnabled = true
    var signCopyDyeGlowWax = false

    var invisibleItemFramesEnabled = true
    var invisibleItemFramesItem = Material.AMETHYST_SHARD

    var langPrefix = MiniMessage.miniMessage()
        .deserialize("<red>[<gold>ImprovedSigns<red>]<reset> ")
    var langSignCopy = MiniMessage.miniMessage()
        .deserialize("<aqua>The contents of this sign have been copied to the sign in your hand!")
    var langSignContentsRetained = MiniMessage.miniMessage()
        .deserialize("<aqua>The contents of the sign you broke have been retained!")
    var langSignContentsRestored = MiniMessage.miniMessage()
        .deserialize("<aqua>The contents of the sign you created have been restored!")
    var langItemFrameVisibilityChange = MiniMessage.miniMessage()
        .deserialize("<aqua>The item frame you right-clicked with <b>{0}</b> is now {1}!")
}
