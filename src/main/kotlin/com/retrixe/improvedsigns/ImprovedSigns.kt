package com.retrixe.improvedsigns

import com.retrixe.improvedsigns.listeners.InvisibleItemFrameListener
import com.retrixe.improvedsigns.listeners.PassthroughListener
import com.retrixe.improvedsigns.listeners.SignCopyListener
import com.retrixe.improvedsigns.listeners.SignEditorListener
import com.retrixe.improvedsigns.listeners.SignsRetainListener
import net.kyori.adventure.text.Component
import net.kyori.adventure.text.format.NamedTextColor
import org.bukkit.plugin.java.JavaPlugin
import java.util.logging.Level

// TODO: Signs disappear when placed in spawn protected area.
// FIXME: Sign copying doesn't work with empty signs.

class ImprovedSigns : JavaPlugin() {
    companion object {
        lateinit var plugin: ImprovedSigns
            private set
    }

    override fun onLoad() {
        plugin = this
    }

    override fun onEnable() {
        Config.reloadConfig()

        getCommand("improvedsigns")?.setTabCompleter { _, _, _, _ -> listOf("reload") }
        getCommand("improvedsigns")?.setExecutor { sender, _, _, args ->
            if (args.size == 1 && args[0] == "reload") {
                try {
                    Config.reloadConfig()
                    sender.sendMessage(Component
                        .text("Successfully reloaded ImprovedSigns config.")
                        .color(NamedTextColor.AQUA))
                } catch (e: Exception) {
                    sender.sendMessage(Component
                        .text("Failed to reload ImprovedSigns config! Check console for error.")
                        .color(NamedTextColor.RED))
                    plugin.logger.log(Level.SEVERE, "An error occurred while loading the config!", e)
                }
                true
            } else false
        }

        // Register all listeners
        server.pluginManager.registerEvents(InvisibleItemFrameListener(), this)
        server.pluginManager.registerEvents(PassthroughListener(), this)
        server.pluginManager.registerEvents(SignCopyListener(), this)
        server.pluginManager.registerEvents(SignEditorListener(), this)
        server.pluginManager.registerEvents(SignsRetainListener(), this)

        // Setup integrations
        // TODO: Integrations with Factions, WorldGuard, BlockLocker, ChestShop, CoreProtect, spawn protection
        // Setup listeners on ItemFrameVisibilityChange/Passthrough/SignEditEvent which check with Factions/WorldGuard.
        // We could also fake events and see if they get cancelled, that could provide integration with more plugins?
        // And maybe even give the same message that WorldGuard/Factions give when there is a denial.
        // For permissions, listen to SignCopyEvent too.
        // CoreProtect has an API which could be used for sign retention/sign editing.
        // Invisible item frames and sign edits could be logged to console, perhaps?

        // Register recipes
        createSignRecipes()
    }
}
