package com.retrixe.improvedsigns.signs

import com.retrixe.improvedsigns.*
import io.github.bananapuncher714.nbteditor.NBTEditor
import net.kyori.adventure.text.Component
import net.kyori.adventure.text.format.NamedTextColor
import net.kyori.adventure.text.format.TextColor
import net.kyori.adventure.text.serializer.gson.GsonComponentSerializer
import org.bukkit.DyeColor
import org.bukkit.block.Sign
import org.bukkit.block.sign.Side
import org.bukkit.inventory.ItemStack
import java.util.*

object SignUtilsPost20 : SignUtils {
    override fun getSignData(sign: Sign, includeDyeGlowWax: Boolean) = SignData(
        includeDyeGlowWax && sign.isWaxed,
        EnumMap<SignSideEnum, SignSideData>(SignSideEnum::class.java).apply {
            val front = sign.getSide(Side.FRONT)
            put(SignSideEnum.FRONT, SignSideData(
                front.lines().toList(),
                if (includeDyeGlowWax) front.color ?: DyeColor.BLACK else DyeColor.BLACK,
                includeDyeGlowWax && front.isGlowingText
            ))
            val back = sign.getSide(Side.BACK)
            put(SignSideEnum.BACK, SignSideData(
                back.lines().toList(),
                if (includeDyeGlowWax) back.color ?: DyeColor.BLACK else DyeColor.BLACK,
                includeDyeGlowWax && back.isGlowingText
            ))
        }
    )

    override fun getSignData(sign: ItemStack): SignData {
        if (NBTEditor.contains(sign, "BlockEntityTag", "Text1") ||
            NBTEditor.contains(sign, "BlockEntityTag", "GlowingText") ||
            NBTEditor.contains(sign, "BlockEntityTag", "Color"))
            return SignUtilsPre20.getSignData(sign)

        val sides = EnumMap<SignSideEnum, SignSideData>(SignSideEnum::class.java)

        for (side in SignSideEnum.entries) {
            val signData = NBTEditor.getNBTCompound(sign,
                NBTEditor.ITEMSTACK_COMPONENTS,
                NBTEditor.CUSTOM_DATA,
                "BlockEntityTag",
                side.nbtName)
            if (signData == null) {
                sides[side] = SignSideData(listOf(), DyeColor.BLACK, false)
                continue
            }
            val texts = arrayListOf<Component>()
            for (i in 0..3) {
                texts.add(deserialiseText(NBTEditor.getString(signData, "messages", i)))
            }
            sides[side] = SignSideData(texts,
                DyeColor.valueOf(NBTEditor.getString(signData, "color").uppercase()),
                NBTEditor.getBoolean(signData, "has_glowing_text"))
        }
        val waxed = NBTEditor.getBoolean(sign, NBTEditor.CUSTOM_DATA, "BlockEntityTag", "is_waxed")
        return SignData(waxed, sides)
    }

    override fun setSignData(sign: Sign, data: SignData): Sign {
        sign.isWaxed = data.waxed
        data.sides[SignSideEnum.FRONT]?.let {
            val front = sign.getSide(Side.FRONT)
            front.isGlowingText = it.glowing
            front.color = it.color
            for (i in 0..3) {
                front.line(i, it.text.getOrNull(i) ?: Component.empty())
            }
        }
        data.sides[SignSideEnum.BACK]?.let {
            val back = sign.getSide(Side.BACK)
            back.isGlowingText = it.glowing
            back.color = it.color
            for (i in 0..3) {
                back.line(i, it.text.getOrNull(i) ?: Component.empty())
            }
        }
        return sign
    }

    override fun setSignData(sign: ItemStack, data: SignData, keepDyeWhenNoText: Boolean): ItemStack {
        var item = sign
        var keepDye = keepDyeWhenNoText // Check if the sign has text.
        val lore = arrayListOf<Component>()

        item = NBTEditor.set(item, data.waxed, NBTEditor.CUSTOM_DATA, "BlockEntityTag", "is_waxed")
        lore.add(Component.text("Waxed: ${if (data.waxed) "Yes" else "No"}"))

        for (entry in data.sides) {
            val name = entry.key.name.lowercase()
                .replaceFirstChar { if (it.isLowerCase()) it.titlecase() else it.toString() }
            lore.add(Component.text(name, NamedTextColor.WHITE).append(
                if (entry.value.glowing) Component.text(" (Glowing)")
                else Component.empty()))

            val sideNbt = NBTEditor.getEmptyNBTCompound()
            val loreColor = TextColor.color(entry.value.color.color.asRGB())
            for (i in 0..3) {
                val line = entry.value.text.getOrNull(i)
                if (line != null && !isComponentBlank(line)) keepDye = true
                sideNbt.set(
                    GsonComponentSerializer.gson().serialize(line ?: Component.empty()),
                    "messages",
                    NBTEditor.NEW_ELEMENT)

                lore.add(Component.text("- ", NamedTextColor.WHITE)
                    .append((line ?: Component.empty()).color(loreColor)))
            }
            sideNbt.set(keepDye && entry.value.glowing, "has_glowing_text")
            sideNbt.set(if (keepDye) entry.value.color.name else DyeColor.BLACK.name, "color")
            item = NBTEditor.set(item, sideNbt, NBTEditor.CUSTOM_DATA, "BlockEntityTag", entry.key.nbtName)
        }

        item.lore(lore)
        return item
    }

    override fun isSignNotEmpty(sign: Sign) =
        sign.getSide(Side.FRONT).let {
            it.isGlowingText ||
                    (it.color != null && it.color != DyeColor.BLACK) ||
                    it.lines().any { line -> !isComponentBlank(line) }
        } || sign.getSide(Side.BACK).let {
            it.isGlowingText ||
                    (it.color != null && it.color != DyeColor.BLACK) ||
                    it.lines().any { line -> !isComponentBlank(line) }
        }
}
