package com.retrixe.improvedsigns.signs

import com.retrixe.improvedsigns.SignData
import org.bukkit.block.Sign
import org.bukkit.inventory.ItemStack

interface SignUtils {
    fun getSignData(sign: Sign, includeDyeGlowWax: Boolean = true): SignData
    fun getSignData(sign: ItemStack): SignData
    fun setSignData(sign: Sign, data: SignData): Sign
    fun setSignData(sign: ItemStack, data: SignData, keepDyeWhenNoText: Boolean = true): ItemStack
    fun isSignNotEmpty(sign: Sign): Boolean
}
