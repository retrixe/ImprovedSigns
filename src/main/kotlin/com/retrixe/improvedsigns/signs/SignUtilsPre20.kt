package com.retrixe.improvedsigns.signs

import com.retrixe.improvedsigns.*
import io.github.bananapuncher714.nbteditor.NBTEditor
import net.kyori.adventure.text.Component
import net.kyori.adventure.text.format.NamedTextColor
import net.kyori.adventure.text.format.TextColor
import net.kyori.adventure.text.serializer.gson.GsonComponentSerializer
import org.bukkit.DyeColor
import org.bukkit.block.Sign
import org.bukkit.inventory.ItemStack
import java.lang.IllegalArgumentException
import java.util.EnumMap

// All sign methods other than getSide(Side) are deprecated and color is @Nullable
@Suppress("DEPRECATION", "USELESS_ELVIS", "SENSELESS_COMPARISON")
object SignUtilsPre20 : SignUtils {
    override fun getSignData(sign: Sign, includeDyeGlowWax: Boolean) = SignData(
        false,
        EnumMap<SignSideEnum, SignSideData>(SignSideEnum::class.java).apply {
            put(SignSideEnum.FRONT, SignSideData(
                sign.lines().toList(),
                if (includeDyeGlowWax) sign.color ?: DyeColor.BLACK else DyeColor.BLACK,
                includeDyeGlowWax && sign.isGlowingText
            ))
        }
    )

    override fun getSignData(sign: ItemStack) = SignData(
        false,
        EnumMap<SignSideEnum, SignSideData>(SignSideEnum::class.java).apply {
            put(SignSideEnum.FRONT, SignSideData(
                listOf(
                    deserialiseText(NBTEditor.getString(sign, "BlockEntityTag", "Text1") ?: ""),
                    deserialiseText(NBTEditor.getString(sign, "BlockEntityTag", "Text2") ?: ""),
                    deserialiseText(NBTEditor.getString(sign, "BlockEntityTag", "Text3") ?: ""),
                    deserialiseText(NBTEditor.getString(sign, "BlockEntityTag", "Text4") ?: "")
                ),
                try {
                    DyeColor.valueOf(NBTEditor.getString(sign, "BlockEntityTag", "Color") ?: "BLACK")
                } catch (e: IllegalArgumentException) {
                    DyeColor.BLACK
                },
                NBTEditor.getBoolean(sign, "BlockEntityTag", "GlowingText")
            ))
        }
    )

    override fun setSignData(sign: Sign, data: SignData): Sign {
        data.sides[SignSideEnum.FRONT]?.let {
            sign.isGlowingText = it.glowing
            sign.color = it.color
            for (i in 0..3) {
                sign.line(i, it.text.getOrNull(i) ?: Component.empty())
            }
        }
        sign.isEditable = true // TODO (low priority): Is this necessary?
        return sign
    }

    // TODO (low priority): The Fabric mod sets all Text properties or none of them
    override fun setSignData(sign: ItemStack, data: SignData, keepDyeWhenNoText: Boolean): ItemStack {
        val front = data.sides[SignSideEnum.FRONT] ?: return sign
        var item = sign
        var keepDye = keepDyeWhenNoText // Check if the sign has text.

        val lore = arrayListOf(Component.text("Contents ").append(
            if (front.glowing) Component.text(" (Glowing)") else Component.empty()))
        val loreColor = TextColor.color(front.color.color.asRGB())

        // Set Text
        for (i in 0..3) {
            val text = front.text.getOrNull(i)
            keepDye = true
            val json =
                if (text != null && !isComponentBlank(text))
                    GsonComponentSerializer.gson().serialize(text)
                else NBTEditor.DELETE
            item = NBTEditor.set(item, json, "BlockEntityTag", "Text${i + 1}")

            lore.add(Component.text("- ", NamedTextColor.WHITE)
                .append(text ?: Component.empty()).color(loreColor))
        }
        item.lore(lore)

        // Set GlowingText
        if (front.glowing && keepDye) {
            item = NBTEditor.set(item, true, "BlockEntityTag", "GlowingText")
        }

        // Set Color (yes, Bukkit and Minecraft DyeColor are compatible)
        if (front.color != DyeColor.BLACK && keepDye) {
            item = NBTEditor.set(item, front.color.toString(), "BlockEntityTag", "Color")
        }
        return item
    }

    override fun isSignNotEmpty(sign: Sign) = sign.isGlowingText ||
            (sign.color != null && sign.color != DyeColor.BLACK) ||
            sign.lines().any { line -> !isComponentBlank(line) }
}
