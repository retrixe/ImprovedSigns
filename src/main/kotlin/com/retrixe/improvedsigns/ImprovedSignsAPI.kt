package com.retrixe.improvedsigns

import net.kyori.adventure.text.Component
import org.bukkit.DyeColor
import org.bukkit.block.Sign
import org.bukkit.inventory.ItemStack

data class SignData(
    val waxed: Boolean,
    val sides: Map<SignSideEnum, SignSideData>
)

enum class SignSideEnum(val nbtName: String) {
    FRONT("front_text"),
    BACK("back_text")
}

data class SignSideData(
    val text: List<Component?>,
    val color: DyeColor,
    val glowing: Boolean
)

fun getSignData(sign: Sign, includeDyeGlowWax: Boolean = true) =
    SignUtils.getSignData(sign, includeDyeGlowWax)

fun getSignData(sign: ItemStack) =
    SignUtils.getSignData(sign)

fun setSignData(sign: Sign, data: SignData) =
    SignUtils.setSignData(sign, data)

fun setSignData(sign: ItemStack, data: SignData, keepDyeWhenNoText: Boolean = true) =
    SignUtils.setSignData(sign, data, keepDyeWhenNoText)
