package com.retrixe.improvedsigns.listeners

import com.retrixe.improvedsigns.*
import org.bukkit.GameMode
import org.bukkit.block.Sign
import org.bukkit.event.EventHandler
import org.bukkit.event.EventPriority
import org.bukkit.event.Listener
import org.bukkit.event.block.BlockPlaceEvent
import org.bukkit.event.player.PlayerInteractEvent

class SignEditorListener : Listener {
    @EventHandler(priority = EventPriority.HIGH)
    fun onSignPlaceEvent(e: BlockPlaceEvent) {
        if (
            // If signs retention is enabled, then block the editor anyway.
            (Config.signEditorBlockOnPlacingSign || Config.signsRetainEnabled) &&
            e.blockPlaced.state is Sign
        ) {
            e.isCancelled = true
            val blockData = e.blockPlaced.blockData
            val location = e.blockPlaced.location
            val type = e.blockPlaced.type
            val signData = getSignData(e.blockPlaced.state as Sign)
            ImprovedSigns.plugin.server.scheduler.scheduleSyncDelayedTask(ImprovedSigns.plugin, {
                if (e.player.gameMode != GameMode.CREATIVE) e.itemInHand.amount -= 1
                location.world.setType(location, type)
                location.world.setBlockData(location, blockData)
                val state = location.block.state
                if (state is Sign) {
                    setSignData(state, signData).update()
                }
            }, 1L)
        }
    }

    @EventHandler
    fun onSignInteractEvent(e: PlayerInteractEvent) {
        val clickedBlock = e.clickedBlock?.state
        if (Config.signEditorEnabled &&
                majorVersion < 20 &&
                clickedBlock is Sign &&
                e.action.isRightClick &&
                e.player.isSneaking &&
                !e.isBlockInHand
        ) {
            @Suppress("DEPRECATION") e.player.openSign(clickedBlock)
        } else if (!Config.signEditorEnabled &&
            majorVersion >= 20 &&
            clickedBlock is Sign &&
            e.action.isRightClick &&
            !e.isBlockInHand
        ) {
            e.isCancelled = true // If sign editor is disabled, block editor.
            // We could also block if editor enabled + player standing, but for now it's ok,
            // sign copy/passthrough listener also block the editor from opening themselves.
        }
    }
}
