package com.retrixe.improvedsigns.listeners

import com.retrixe.improvedsigns.Config
import com.retrixe.improvedsigns.majorVersion
import com.retrixe.improvedsigns.openBlock
import com.retrixe.improvedsigns.signs
import org.bukkit.block.Sign
import org.bukkit.block.data.type.WallSign
import org.bukkit.entity.ItemFrame
import org.bukkit.event.EventHandler
import org.bukkit.event.Listener
import org.bukkit.event.player.PlayerInteractEntityEvent
import org.bukkit.event.player.PlayerInteractEvent

class PassthroughListener : Listener {
    @EventHandler
    fun onSignInteractEvent(e: PlayerInteractEvent) {
        val clickedBlock = e.clickedBlock ?: return
        val blockState = clickedBlock.state
        val blockData = blockState.blockData
        if (Config.passthroughSigns &&
                e.action.isRightClick &&
                blockState is Sign &&
                blockData is WallSign &&
                (!Config.signEditorEnabled || !e.player.isSneaking) &&
                // TODO (low priority): Allow right clicking with sign if copy off/not sneaking
                !signs.contains(e.player.inventory.itemInMainHand.type)) {
            val found = openBlock(e.player, clickedBlock.getRelative(blockData.facing.oppositeFace))
            if (majorVersion >= 20 && found) e.isCancelled = true // Block sign editor from opening.
        }
    }

    @EventHandler(ignoreCancelled = false)
    fun onItemFrameInteractEvent(e: PlayerInteractEntityEvent) {
        val entity = e.rightClicked
        if (Config.passthroughItemFrames &&
                entity is ItemFrame &&
                !e.player.isSneaking &&
                (!Config.invisibleItemFramesEnabled ||
                        e.player.inventory.itemInMainHand.type != Config.invisibleItemFramesItem)
            ) {
                e.isCancelled = true
                openBlock(e.player, entity.location.add(entity.attachedFace.direction).block)
        }
    }
}
