package com.retrixe.improvedsigns.listeners

import com.retrixe.improvedsigns.Config
import com.retrixe.improvedsigns.isComponentBlank
import net.kyori.adventure.text.TextReplacementConfig
import org.bukkit.GameMode
import org.bukkit.entity.ItemFrame
import org.bukkit.event.EventHandler
import org.bukkit.event.Listener
import org.bukkit.event.player.PlayerInteractEntityEvent

class InvisibleItemFrameListener : Listener {
    @EventHandler
    fun onItemFrameInteractEvent(e: PlayerInteractEntityEvent) {
        val rightClicked = e.rightClicked
        if (Config.invisibleItemFramesEnabled &&
                rightClicked is ItemFrame &&
                rightClicked.isVisible &&
                !e.player.isSneaking &&
                e.player.inventory.itemInMainHand.type == Config.invisibleItemFramesItem) {
            if (e.player.gameMode != GameMode.CREATIVE) e.player.inventory.itemInMainHand.amount -= 1
            rightClicked.isVisible = false
            e.isCancelled = true

            if (!isComponentBlank(Config.langItemFrameVisibilityChange)) {
                e.player.sendMessage(Config.langPrefix
                    .append(Config.langItemFrameVisibilityChange
                        .replaceText(TextReplacementConfig.builder()
                            .matchLiteral("{0}")
                            .replacement(Config.invisibleItemFramesItem.name.lowercase().replace("_", " "))
                            .build())
                        .replaceText(TextReplacementConfig.builder()
                            .matchLiteral("{1}")
                            .replacement("invisible")
                            .build())))
            }
        }
    }
}
