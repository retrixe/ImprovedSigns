package com.retrixe.improvedsigns.listeners

import com.retrixe.improvedsigns.*
import org.bukkit.block.Sign
import org.bukkit.event.EventHandler
import org.bukkit.event.Listener
import org.bukkit.event.player.PlayerInteractEvent

class SignCopyListener : Listener {
    @EventHandler
    fun onSignInteractEvent(e: PlayerInteractEvent) {
        val clickedBlock = e.clickedBlock?.state
        if (Config.signCopyEnabled &&
                clickedBlock is Sign &&
                e.action.isRightClick &&
                !e.player.isSneaking &&
                signs.contains(e.player.inventory.itemInMainHand.type)) {
            if (majorVersion >= 20) e.isCancelled = true // Block sign editor from opening.

            val signData = getSignData(clickedBlock, Config.signCopyDyeGlowWax)
            val updatedSign = setSignData(e.player.inventory.itemInMainHand, signData)
            e.player.inventory.setItemInMainHand(updatedSign)

            if (!isComponentBlank(Config.langSignCopy)) {
                e.player.sendMessage(Config.langPrefix
                    .append(Config.langSignCopy))
            }
        }
    }
}
