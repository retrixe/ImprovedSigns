package com.retrixe.improvedsigns.listeners

import com.retrixe.improvedsigns.*
import org.bukkit.Material
import org.bukkit.block.Sign
import org.bukkit.event.EventHandler
import org.bukkit.event.EventPriority
import org.bukkit.event.Listener
import org.bukkit.event.block.BlockDropItemEvent
import org.bukkit.event.block.BlockPlaceEvent
import org.bukkit.inventory.ItemStack

class SignsRetainListener : Listener {
    @EventHandler(priority = EventPriority.LOW)
    fun onSignPlaceEvent(e: BlockPlaceEvent) {
        val blockState = e.block.state
        if (Config.signsRetainEnabled && blockState is Sign) {
            val signData = getSignData(e.itemInHand)
            val state = setSignData(blockState, signData)
            state.update()

            if (!isComponentBlank(Config.langSignContentsRestored) && isSignNotEmpty(state)) {
                e.player.sendMessage(Config.langPrefix
                    .append(Config.langSignContentsRestored))
            }

            if (Config.signEditorBlockOnPlacingSign || signData.waxed) return
            // Open sign editor 2 ticks later (since the sign will be placed 1 tick later
            // by the sign editor blocking listener in SignEditorListener).
            ImprovedSigns.plugin.server.scheduler.scheduleSyncDelayedTask(ImprovedSigns.plugin, {
                // This is necessary for wall signs, for some reason.
                val currentState = e.block.state
                if (currentState is Sign) {
                    @Suppress("DEPRECATION") e.player.openSign(currentState)
                }
            }, 2L)
        }
    }

    @EventHandler
    fun onSignBreakEvent(e: BlockDropItemEvent) {
        val blockState = e.blockState
        if (Config.signsRetainEnabled && blockState is Sign && e.items.size >= 1) {
            if (!isSignNotEmpty(blockState)) return

            val signMaterial = Material.valueOf(blockState.type.toString().replace("WALL_", ""))
            e.items[0].itemStack = setSignData(
                ItemStack(signMaterial),
                getSignData(blockState),
                Config.signsRetainKeepDyeWhenNoText
            )

            if (!isComponentBlank(Config.langSignContentsRetained)) {
                e.player.sendMessage(Config.langPrefix
                    .append(Config.langSignContentsRetained))
            }
        }
    }
}
